<!DOCTYPE html>
<html>
<head>
		<title>Transaksi</title>
	<meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/img/logo.png" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet"/>
</head>
<body>
	@extends('layout.main')
	<section class="page-section" id="contact">
            <div class="container mt-5">
                <!-- Contact Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0" >Transaksi</h2>
                <!-- Icon Divider-->
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-crown"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- Contact Section Form-->
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                        <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19.-->
                       <form action="/transaksi/store" method="POST">
                        @csrf
                         <div class="control-group">
                                <div class="form-group">
                                    <label for="nama">Nama Bunga</label>
                                    <input class="form-control  floating-label-form-group controls mb-0 pb-2" name="nama_bunga" type="text" value="" disabled="true"  />
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label for="nama_pelanggan">Nama Pelanggan</label>
                                    <input class="form-control  floating-label-form-group controls mb-0 pb-2" name="nama_pelanggan" type="nama pelanggan" placeholder="Nama Pelanggan"/>
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label for="harga">Harga</label>
                                    <input class="form-control  floating-label-form-group controls mb-0 pb-2" name="harga" type="tel" disabled="true" value="" />
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label for="jumlah_pembelian">Jumlah Pembelian</label>
                                    <input class="form-control  floating-label-form-group controls mb-0 pb-2" name="jumlah_pembelian" type="tel" placeholder="Jumlah Pembelian"/>
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label for="potongan_harga">Potongan Harga</label>
                                    <input class="form-control  floating-label-form-group controls mb-0 pb-2" name="potongan_harga" type="tel" placeholder="Potongan Harga"/>
                                </div>
                            </div>
                            <div>
                                <div class="form-group">
                                    <label for="total_harga">Total Harga</label>
                                    <input class="form-control  floating-label-form-group controls mb-0 pb-2" name="total_harga" type="tel" placeholder="Total Harga"/>
                                </div>
                            </div>
                            <br />
                            <div id="success"></div>
                            <div class="form-group"><button class="btn btn-primary btn-xl" id="sendMessageButton" type="submit">Transaksi</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
</body>
</html>