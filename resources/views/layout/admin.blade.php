<!DOCTYPE html>
<html>
<head>
    <title>
        
    </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
     <form action="/admin/store" method="POST" enctype="multipart/form-data">
     @csrf
        <div class="form-group">
            <label for="nama_bunga">Nama Bunga</label>
            <input type="nama" class="form-control" name="nama_bunga" aria-describedby="emailHelp" placeholder="Nama Bunga">
        </div>
          <div class="form-group">
            <label for="harga">Harga</label>
            <input type="harga" class="form-control" name="harga" placeholder="Harga">
        </div>
        <div class="form-group">
            <label for="bunga_image">Foto</label>
            <input type="file" class="form-control" name="bunga_image" placeholder="Foto">
        </div>

          <button type="submit" class="btn btn-primary">Submit</button>
        </form>

</body>
</html>