<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BungaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','BungaController@index');
Route::get('/admin',function(){
	return view('layout.admin');
});
Route::get('/create', 'BungaController@create');
Route::post('/store', 'BungaController@store');
Route::post('/transaksi/store', 'TransaksiController@store');
Route::get('/transaksi/create', 'TransaksiController@create');
Route::post('/admin/store', 'BungaController@store');
Route::get("/bunga/cari", "BungaController@cari");