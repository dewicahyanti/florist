<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Bunga;
use App\transaksi;

class BungaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bunga = Bunga::all();
        return view ('master')->with('bunga',$bunga);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transaksi');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
        'bunga_image' => 'required',
    ]);
        $file = $request->file('bunga_image');
        $bunga = new Bunga;
        $bunga->nama_bunga = $request->nama_bunga;
        $bunga->harga = $request->harga;
        $bunga->bunga_image = $file->getClientOriginalName();
        $tujuan_upload = 'image';
        $file->move($tujuan_upload, $file->getClientOriginalName());
        $bunga->save();

        return redirect('/')->with('msg', 'Data Berhasil di Simpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function cari(Request $request)
    {
        // // menangkap data pencarian
        $cari = $request->cari;
 
        //     // mengambil data dari table pegawai sesuai pencarian data
        $bunga = DB::table('bunga')
                ->where('nama_bunga','like',"%".$cari."%")
                ->paginate();
        //     // mengirim data pegawai ke view index
        return view('master',['bunga' => $bunga]);
 
    }
}
