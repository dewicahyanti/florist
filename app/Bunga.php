<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bunga extends Model
{
   protected $table = 'bunga';
   protected $primarKey = 'id_bunga';
   protected $fillable = ['nama_bunga', 'harga'];
}
