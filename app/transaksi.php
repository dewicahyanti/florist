<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class transaksi extends Model
{
   protected $table = 'transaksi';
   protected $fillable = ['nama_bunga', 'nama_pelanggan', 'harga', 'jumlah_pembelian', 'potongan_harga', 'total_harga'];
}
